package main

import "testing"
import "github.com/stretchr/testify/assert"

func TestHello(t *testing.T) {
	expectedResult := "Hello, world."
	result := Hello()
	assert.Equal(t, expectedResult, result, "Non-matching results.")
}
