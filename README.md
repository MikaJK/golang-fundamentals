# golang-fundamentals

## Installation

Download Golang version 1.11 or newer from https://https://golang.org/dl/. Version 1.11 or newer is required, because it supports Golang modules.

Install the package, set Golang bin directory (e.g. C:\go\bin) to PATH environment variable.

Create a directory as your Golang workspace and set GOPATH environment variable to point into into that directory.

## Working Directory

Create woking directory (e.g. hello) to your workspace.

## Modules and Dependency Management

Execute following command in your working directory to initialize the Golang modules:

    go mod init example.com/main

## Hello World Application

Create hello.go file into your working directory with following content:

    package main

    import "fmt"

    func main() {
        fmt.Printf(Hello())
    }

    func Hello() string {
        return "Hello, world."
    }

The main() function calls Hello() function, which returns "Hello, world." string, which is printed to the screen.

Run Hello World application:

    go run .\hello.go

## External Dependency

Update hello.go to have the following content:

    package main

    import "fmt"
    import "rsc.io/quote"

    func main() {
        fmt.Printf(Hello())
    }

    func Hello() string {
        return quote.Hello()
    }

The Hello() method in rsc.io/quote produces also "Hello, world." string.

Run Hello World application and go mod downloads all required dependencies. Alternative way is to run "go mod download" command to fulfill the dependencies.

With "go mod verify" and "go mod tidy" it is possible to verify that all required dependencies are in placed, and also remove any unused dependencies.

## Testing

First get "github.com/stretchr/testify" package that provides assertion capabilities to testing:

    go get github.com/stretchr/testify

Create hello_test.go file into your working directory with following content:

    package main

    import "testing"
    import "github.com/stretchr/testify/assert"

    func TestHello(t *testing.T) {
        expectedResult := "Hello, world."
        result := Hello()
        assert.Equal(t, expectedResult, result, "Non-matching results.")
    }

The "testing" is internal Golang testing package and "github.com/stretchr/testify/assert" adds assertion capabilities.

Execute tests:

    go test