module pikecape.com/main

go 1.13

require (
	github.com/stretchr/testify v1.4.0
	rsc.io/quote v1.5.2
)
